import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import './App.css';
import { decreaseCount, increaseCount } from './store/Counter/actions';

function App() {
  const count = useSelector(state => state['counterReducer'].count)
  const dispatch = useDispatch()
  console.log(count);


  return (
    <div className="App">
      <h1>count: {count} </h1>
      <button onClick={() => dispatch(increaseCount())}>Increase</button>
      <button onClick={() => {
        (count > 0) ? dispatch(decreaseCount()) : alert('Negative'); return false
      }}>Decrease</button>
    </div>
  );
}

export default App;
