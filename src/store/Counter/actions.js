import {INCREASE_COUNTER,DECREASE_COUNTER} from './actionTypes';

export const increaseCount = () => {
    return {
        type: INCREASE_COUNTER
    }
}

export const decreaseCount = () => {
    return {
        type: DECREASE_COUNTER
    }
}

