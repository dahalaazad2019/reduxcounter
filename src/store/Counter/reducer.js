import * as actions from './actionTypes'

const initialState = {
    count:0
}

export const counterReducer = (state = initialState, action) => {
    switch (action.type) {
        case actions.INCREASE_COUNTER:
            return {
                ...state,
                count:state.count + 1 
            }
        case actions.DECREASE_COUNTER:
            return {
                ...state,
                count: state.count - 1
            }
        default:
            return state;
    }
}